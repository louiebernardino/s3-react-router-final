import React, { useState } from 'react';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import { Link } from 'react-router-dom';

const RegisterForm = (props) => {
//STATE
//object that holds a component's dynamic data

//useState
const [username, setUsername] = useState("test");
console.log(username);
  return (
    <Form className="m-3">
      <FormGroup>
        <Label for="username">Username</Label>
        <Input type="username" name="username" id="username" value={username} onChange={e => setUsername(e.target.value)}/>
      </FormGroup>
      <FormGroup>
        <Label for="email">Email</Label>
        <Input type="email" name="email" id="email"/>
      </FormGroup>
      <FormGroup>
        <Label for="password">Password</Label>
        <Input type="password" name="password" id="password"/>
      </FormGroup>
      <FormGroup>
        <Label for="password2">Password</Label>
        <Input type="password2" name="password2" id="password2"/>
      </FormGroup>
      <Button color="primary" size="sm" block className="mb-3">Submit</Button>
      <p>
        Already have an account? <Link to="/login">click here</Link>
      </p>
    </Form>
  );
}

export default RegisterForm;