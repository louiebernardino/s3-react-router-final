import React from 'react';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';

const TeamForm = (props) => {
  return (
    <Form className="m-3">
      <FormGroup>
        <Label for="teamSelect">Team</Label>
        <Input type="select" name="teamSelect" id="teamSelect">
          <option>A</option>
          <option>B</option>
          <option>C</option>
          <option>D</option>
          <option>E</option>
        </Input>
      </FormGroup>
      <FormGroup>
        <Label for="idSelect">ID</Label>
        <Input type="idSelect" name="idSelect" id="idSelect"/>
      </FormGroup>
      <Button color="primary" size="sm" block>Save Changes</Button>
    </Form>
  );
}

export default TeamForm;