//Dependencies
import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import TaskForm from '../forms/TaskForm';
import TaskTable from '../tables/TaskTable';


//COMPONENTS
const TasksPage = (props) => { //props - arguments
/*anonymous function*/
//JSX
  return (
    <Container>
      <Row className="mb-4">
        <Col>
          <h1>Tasks Page</h1>
        </Col>
      </Row>
      <Row>
        <Col md="4" className="border"><TaskForm/></Col>
        <Col><TaskTable/></Col>
      </Row>
    </Container>
  );
}

export default TasksPage;

//Discussion
//1.) What is a component?
// - function or class.
// - contains props (arguments)