import React from 'react';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';

const LoginPage = (props) => {
  return (
    <Form className="m-3">
      <FormGroup>
        <Label for="taskDescription">Username</Label>
        <Input type="taskDescription" name="taskDescription" id="taskDescription"/>
      </FormGroup>
      <FormGroup>
        <Label for="memberId">Password</Label>
        <Input type="password" name="memberId" id="memberId"/>
      </FormGroup>
      <Button color="primary" size="sm" block>Login</Button>
    </Form>
  );
}

export default LoginPage;