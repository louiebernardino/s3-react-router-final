import React from 'react';
import { Table } from 'reactstrap';
import TaskRow from '../rows/TaskRow'
import { Container, Row, Col } from 'reactstrap';


const MemberTable = (props) => {
  return (
    <Table hover borderless sie="sm" className="p-4 rounded">
      <thead>
        <tr>
          <th>#</th>
          <th>Member Id</th>
          <th>Description</th>
          <th>Action</th>
        </tr>
      </thead>
        <tbody>
          <TaskRow/>
        </tbody>
    </Table>
  );
}

export default MemberTable;