import React from 'react';
import { Table } from 'reactstrap';
import MemberRow from '../rows/MemberRow';
import { Container, Row, Col } from 'reactstrap';
import TeamRow from '../rows/TeamRow';


const TeamTable = (props) => {
  return (
    <Table hover borderless sie="sm" className="p-4 rounded">
      <thead>
        <tr>
          <th>#</th>
          <th>Team</th>
          <th>Username</th>
          <th>Email</th>
          <th>Position</th>
          <th>Action</th>
        </tr>
      </thead>
        <tbody>
          <TeamRow/>
        </tbody>
      <thead>
      </thead>
    </Table>
  );
}

export default TeamTable;